/* Mobile menu js */
$(document).ready(function(){
    //hamburger Toggle
    $('.humbarger-menu').click(function(e){
	    $('.header-menu-inner').slideToggle(500);
	    $('.humbarger-menu').toggleClass('close-icon-active');
	    e.preventDefault();
    
	    $('.header-menu-inner li a').click(function(e) {
	        if ($(window).width() < 769) {
	          $('.header-menu-inner').slideUp(500);
	          e.preventDefault(); 
	        }
	  	});
	});
});


/* Slider custom js */

$(document).ready(function(){
	$('.testimonial-slider').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		prevArrow: false,
    	nextArrow: false
	});
});